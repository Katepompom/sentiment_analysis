__author__ = 'xead'
from sklearn.externals import joblib
import string
import numpy as np
import re

class SentimentClassifier(object):
    def __init__(self):
        self.model = joblib.load("./SVMTextSentiment.pkl")
        self.classes_dict = {0: '\U0001F621', 1: '\U0001F60A', -1: 'ошибка оценки \U0001F623'}

    def predict_text(self, text):
         try:
            cleared_text = self.clean_data(text)
            return self.model.predict(cleared_text)[0]
         except:
            print('ошибка оценки')
            return -1

    def predict_list(self, list_of_texts):
        try:
            cleared_texts = clean_data(list_of_texts)
            return self.model.predict(cleared_texts)
        except:
            print('ошибка оценки')
            return -1

    def get_prediction_message(self, text):
        prediction = self.predict_text(text)
        return self.classes_dict[prediction]
    
    def clean_data(self, list_of_texts):
        cleared_texts = list_of_texts.copy()
        replace_punctuation = str.maketrans(string.punctuation, ' '*len(string.punctuation))
        for i in np.arange(len(cleared_texts)):
    #       приведение к нижнему регистру и замена "ё" на "е"
            cleared_texts[i] = cleared_texts[i].lower().replace("ё", "е")
    #       очистка от цифр и английских слов
            cleared_texts[i] = re.sub(r"[A-Za-z0-9]", " ", cleared_texts[i])
    #       очистка от переносов строк
            cleared_texts[i] = re.sub(r"\r\n|\r|\n", " ", cleared_texts[i])
    #       очистка от пунктуации 
            cleared_texts[i] = cleared_texts[i].translate(replace_punctuation)
    #       очистка от лишних пробелов  
            cleared_texts[i] = " ".join(cleared_texts[i].split())
        return cleared_texts