if __name__ == '__main__':

    import requests
    import bs4
    from multiprocessing import Pool
    import codecs
    import parse_module
    from functools import reduce

    p = Pool(5)
    course_list = ['wall-street-english', 'anchor-training', 'skyeng', 'star-talk', 'easy-speak']
#   course_list = ['skyeng']
    url_list = [
        ('https://msk.englishchoice.ru/courses/' + name + '/comments/', name)
        for name in course_list
    ]

    map_results = p.starmap(parse_module.parse_page_courses, url_list)
    reduce_results = reduce(lambda x, y: x + y, map_results)
    with codecs.open('parsing_courses_results.txt', 'w',
                     'utf-8') as output_file:
        print(u'\n'.join(reduce_results), file=output_file)
