import requests
import bs4
    
def parse_page(url):
    text = requests.get(url).text
    parser = bs4.BeautifulSoup(text, 'lxml')
    x = parser.findAll('div', attrs={'class': 'text'})
    return [res.text for res in x]

def parse_page_courses(url, course_name):
    text = requests.get(url).text
    skyeng_parser = bs4.BeautifulSoup(text, 'lxml')
    x = skyeng_parser.findAll('div',
                              attrs={'class': 'comment-content'})
    ignore_next = False
    result = [course_name, "\n"]
    for el in x:
        result.append(el.p.text)
        result.append("\n------\n")
    return result
