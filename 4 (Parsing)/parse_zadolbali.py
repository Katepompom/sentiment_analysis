
if __name__ == '__main__':
    from multiprocessing import Pool
    import codecs
    import parse_module
    from functools import reduce
    
    p = Pool(10)
    url_list = [
        'http://zadolba.li/201604' + '0' * int(n < 10) + str(n)
        for n in range(1, 2)
    ]

    map_results = p.map(parse_module.parse_page, url_list)
    reduce_results = reduce(lambda x, y: x + y, map_results)
    with codecs.open('parsing_results.txt', 'w', 'utf-8') as output_file:
        print(u'\n'.join(reduce_results), file=output_file)
