# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from mail.items import PhoneItem
from scrapy.selector import Selector
import re

def process_url(value):
    return re.sub(r'[#]', '', re.sub(r'[?]page=\d+[#]', '', value))

class MailCrawlerSpider(CrawlSpider):
    name = 'mail_crawler'
    allowed_domains = ["hi-tech.mail.ru"]
    start_urls = [
        "https://hi-tech.mail.ru/mobile-catalog/"
    ]

    rules = [
        Rule(LinkExtractor(restrict_xpaths='//div[@class="paging js-paging"]/a[@class="paging__link paging__link_nav paging__link_nav_next js-paging__control padding_0"]', process_value = process_url),
             callback='parse_item', follow=True)]
    
    def parse_item(self, response):
        print("---->" + response.url)
        phones = response.xpath('//div[@class="p-catalog-card p-catalog-card_vertical"]/div[@class="p-catalog-card__info"]/div[@class="p-catalog-card__title"]')
        if len(phones) == 0:
            yield PhoneItem()
        for phone in phones:
            item = PhoneItem()
            item['phone_name'] = phone.xpath(
                'a/text()').extract()[0]
            item['phone_url'] = phone.xpath('a/@href').extract()[0]
            yield item
           
            
