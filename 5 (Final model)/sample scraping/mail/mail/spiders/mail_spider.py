from scrapy import Spider
from scrapy.selector import Selector

from mail.items import PhoneItem


class MailSpider(Spider):
    name = "mail"
    allowed_domains = ["hi-tech.mail.ru"]
    start_urls = [
        "http://hi-tech.mail.ru/mobile-catalog/",
    ]
    
    def parse(self, response):
        phones = response.xpath('//div[@class="p-catalog-card p-catalog-card_vertical"]/div[@class="p-catalog-card__info"]/div[@class="p-catalog-card__title"]')
        for phone in phones:
            item = PhoneItem()
            item['phone_name'] = phone.xpath(
                'a/text()').extract()[0]
            item['phone_url'] = phone.xpath('a/@href').extract()[0]
            yield item