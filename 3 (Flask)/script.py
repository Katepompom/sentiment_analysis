# Для предсказания класса отзыва (позитивный/негативный) используется SVM-классификатор с TF-IDF преобразователем текста в признаки, построенном на базе тренировочного сета products_sentiment_train.tsv.

from sentiment_classifier import SentimentClassifier
from flask import Flask, render_template, request

app = Flask(__name__)

print ("Preparing classifier")
classifier = SentimentClassifier()
print ("Classifier is ready")

@app.route("/", methods=["POST", "GET"])
def index_page(text="", prediction_message=""):
    if request.method == "POST":
        text = request.form["text"]
        print (text)
        prediction_message = classifier.get_prediction_message([text])
        print (prediction_message)
    return render_template('simple template.html', text=text, prediction_message=prediction_message)

if __name__ == "__main__":
    app.run()


