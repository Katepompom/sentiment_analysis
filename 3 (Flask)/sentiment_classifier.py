__author__ = 'xead'
from sklearn.externals import joblib


class SentimentClassifier(object):
    def __init__(self):
        self.model = joblib.load("./SVMTextSentiment.pkl")
#       self.vectorizer = joblib.load("./TfidfVectorizer.pkl")
        self.classes_dict = {0: "negative", 1: "positive", -1: "prediction error"}

    def predict_text(self, text):
#         try:
#           vectorized = self.vectorizer.text([text])
            return self.model.predict(text)[0]
#         except:
#             print("prediction error")
#             return -1, 0.8

    def predict_list(self, list_of_texts):
        try:
#           vectorized = self.vectorizer.transform(list_of_texts)
            return self.model.predict(list_of_texts)
        except:
            print('prediction error')
            return None

    def get_prediction_message(self, text):
        prediction = self.predict_text(text)
        return self.classes_dict[prediction]